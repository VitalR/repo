import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static sun.management.Agent.getText;

/**
 * Created by vrodikov on 23/05/2017.
 */
public class MyFirstTest {

    private WebDriver driver;

    @BeforeTest
    public void setup() {
        final File file = new File(PropertyLoader.loadProperty("path.webDriver"));
        System.setProperty(PropertyLoader.loadProperty("webDriver"), file.getAbsolutePath());
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }


    @Test
    public void goToPage() {
//      setup();

        driver.navigate().to("http://juliemr.github.io/protractor-demo/");
        Assert.assertEquals("http://juliemr.github.io/protractor-demo/", driver.getCurrentUrl());

        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);

        System.out.println("The Page is loaded");

    }

    @Test(dependsOnMethods = "goToPage")
    public void inputFirstValue() {

        WebElement inputElement1 = driver.findElement(By.xpath("//input [@ng-model='first']"));
        inputElement1.sendKeys("10");

        Assert.assertEquals(inputElement1.getAttribute("value"), "10");

        System.out.println("Input first value: 10");

    }

    @Test(dependsOnMethods = "inputFirstValue")
    public void inputSecondValue() {

        //WebElement inputElement1 = driver.findElement(By.tagName("first"));
        WebElement inputElement2 = driver.findElement(By.xpath("//input [@ng-model='second']"));
        inputElement2.sendKeys("12");

        Assert.assertEquals(inputElement2.getAttribute("value"), "12");

        System.out.println("Input second value: 12");

    }

    @Test(dependsOnMethods = "inputSecondValue")
    public void setOperator() {

        Select dropdown = new Select(driver.findElement(By.xpath("//select [@ng-model='operator']")));
        dropdown.selectByValue("SUBTRACTION");

        Assert.assertEquals(dropdown.getFirstSelectedOption().getText(), "-");

        System.out.println("Select operator: SUBTRACTION");

    }

    @Test(dependsOnMethods = "setOperator")
    public void clickOn() {

        WebElement button = driver.findElement(By.xpath("//button[@id='gobutton']"));
        button.click();

        try {
            Thread.sleep(1800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement result = driver.findElement(By.xpath("//h2"));

        Assert.assertEquals(result.getText(), "-2");

        System.out.println("Get result: 10 - 12 = -2");

    }

    /*
    @AfterTest
    public void test2(){
        driver.close();
    }
    */
}
